<?php

namespace Drupal\pl9nav;

/**
 * Manipulate the main menu to provide Project Light nav structure.
 */
class MainMenuTreeManipulators {

  /**
   * Ensure the Home menu item is at the top of the menu tree.
   */
  public function promoteHomeMenuItem($tree) {
    $new_tree = [];

    foreach ($tree as $id => $item) {
      $url = $item->link->getUrlObject();
      if ($url->isRouted() && $url->getRouteName() === '<front>') {
        $front_id = $id;
      }
    }

    if (!isset($front_id)) {
      // If we could not find front_id, we should make no further changes
      // to the tree.
      return $tree;
    }

    $front_link = $tree[$front_id];

    unset($tree[$front_id]);
    // We manipulate the link after responsive_menu has ensured that Drupal's
    // generateIndexAndSort() has run. We set a key of zero in case any other
    // manipulators get added but we are explictly putting $front_link as the
    // first element of $new_tree.
    $new_tree = [0 => $front_link] + $tree;
    return $new_tree;
  }

}
